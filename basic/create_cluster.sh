#!/bin/sh

# configuration cluster parameters
# this file version only support create one master
master_nodes_count=1
work_nodes_count=2

# create master hosts
for (( c=1; c<=$master_nodes_count; c++ ))
do
  nodename=master$(printf "%02d" "$c")
  multipass launch --name $nodename
  multipass exec $nodename -- bash -c "curl -sfL https://get.k3s.io | sh -"
done

# get master node informaton
TOKEN=$(multipass exec master01 sudo cat /var/lib/rancher/k3s/server/node-token)
IP=$(multipass info master01 | grep IPv4 | awk '{print $2}')
multipass exec master01 sudo cat /etc/rancher/k3s/k3s.yaml > k3s.yaml
sed -i '' "s/127.0.0.1/$IP/" k3s.yaml

# create work hosts
for (( c=1; c<=$work_nodes_count; c++ ))
do
  nodename=work$(printf "%02d" "$c")
  multipass launch --name $nodename
  multipass exec $nodename -- bash -c "curl -sfL https://get.k3s.io | K3S_URL=\"https://$IP:6443\" K3S_TOKEN=\"$TOKEN\" sh -"
done

# setting kubeconfig
echo
echo "K3s cluster is ready !"
echo
echo "Run the following command to set the current context:"
echo "$ export KUBECONFIG=$PWD/k3s.yaml"
echo
echo "and start to use the cluster:"
echo  "$ kubectl get nodes"
echo