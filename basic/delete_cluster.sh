#!/bin/sh

# configuration cluster parameters
# this file version only support create one master
master_nodes_count=1
work_nodes_count=2

for (( c=1; c<=$master_nodes_count; c++ ))
do
  nodename=master$(printf "%02d" "$c")
  multipass delete $nodename
done

# delete work hosts
for (( c=1; c<=$work_nodes_count; c++ ))
do
  nodename=work$(printf "%02d" "$c")
  multipass delete $nodename
done

multipass purge
rm k3s.yaml
